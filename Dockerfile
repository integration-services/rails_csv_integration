FROM registry.gitlab.com/fullmeasure/public/docker/rails-mysql-node8:latest

RUN mkdir /current
WORKDIR /current
ADD . /current

RUN bundle install

EXPOSE 3000
