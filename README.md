# Rails CSV Integration

This repo generates a docker file that:
 1) downloads csvs from a FTP resource 
 1) manages changes to the csvs over time via a persisted sqlite database
 1) upserts the relevant information to the Full Measure Web Services
