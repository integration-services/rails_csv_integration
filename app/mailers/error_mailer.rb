class ErrorMailer < ApplicationMailer
  def error(to: nil, attachment: nil)
    @to = to
    mail.attachments['errors.zip'] = { mime_type: 'application/zip', content: attachment } unless attachment.nil?

    mail(to: @to, subject: 'Full Measure Integration Errors')
  end
end
