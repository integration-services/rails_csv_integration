class Audience < ApplicationRecord
  after_update :update_audience_ids
  after_initialize :set_defaults
  delegate :s3_path, to: :csv_file

  scope :dirty, -> { where(dirty: true) }

  def set_defaults
    self.dirty = true if dirty.nil?
    self.status ||= 'PENDING CREATE'
  end

  def payload
    {
      name: name,
      s3_path: s3_path,
      source: 'csv_integration'
    }
  end

  def csv_file
    @csv_file ||= AudienceContactCsv.find_by_fullmeasure_table_name(name)
  end

  def update_audience_ids
    csv_file.fullmeasure_model_class.update_all(audience_id: fullmeasure_id)
  end

  def soft_delete
    if status == 'PENDING DELETE' &&  !dirty
      destroy!
    else
      update!(status: 'PENDING DELETE', dirty: true)
    end
  end
end
