class AudienceContact < Dynamic
  def payload
    @payload ||= { external_student_id: contact_id, audience_id: audience_id }
  end

end
