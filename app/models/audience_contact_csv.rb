class AudienceContactCsv < CsvFile

  def upsert_tables
    super
    audience.save!
  end

  def audience
    @audience ||= Audience.find_or_initialize_by(name: fullmeasure_table_name)
  end

  def soft_delete
    audience.soft_delete
    audience.update!(status: 'PENDING DELETE', dirty: true) unless
    fullmeasure_model_class.where.not(status: 'PENDING DELETE', dirty: false).update_all(dirty: true, status: 'PENDING DELETE')
    if fullmeasure_model_class.where(dirty: true).count.zero?
      drop_tables
      destroy!
    end
  end

  def model_class_parent
    AudienceContact
  end
end
