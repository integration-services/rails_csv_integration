class Contact < Dynamic
  delegate :payload, to: :contact_payload_service

  def contact_payload_service
    Api::ContactPayloadService.new(contact: self)
  end
end
