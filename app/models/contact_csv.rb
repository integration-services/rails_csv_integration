class ContactCsv < CsvFile
  after_destroy :drop_tables

  def model_class_parent
    Contact
  end
end
