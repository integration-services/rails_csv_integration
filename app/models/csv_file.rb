require 'base64'
class CsvFile < ApplicationRecord
  delegate :upsert_tables, to: :import_service
  delegate :drop_table, :table_exists?, to: ActiveRecord::Migration
  has_many :audit_csv_files
  validates :name, :type, presence: true

  after_initialize :set_defaults
  before_save :update_file_sha
  before_save :drop_table_if_header_sha_changed, if: :header_sha_changed?
  after_commit :upsert_tables, :audit_sha, on: [:create, :update]

  def set_defaults
    self.fullmeasure_table_name ||= "#{name.remove(/\.csv$/)}"
    self.school_table_name ||= "temp_#{fullmeasure_table_name}"
    self.local_path ||= "#{ENV['PROJECT_DIR']}/csv/#{name}"
    self.s3_path ||= "#{ENV['CI_PROJECT_NAME']}/#{ENV['CI_COMMIT_REF_NAME']}/#{name}"
  end

  def fullmeasure_model_class
    @model_class ||= create_model_class(fullmeasure_table_name)
  end

  def school_model_class
    @school_model_class ||= create_model_class(school_table_name)
  end

  protected

  def drop_table_if_header_sha_changed
    if table_exists?(fullmeasure_table_name)
      drop_table(fullmeasure_table_name)
    end
  end

  def update_file_sha
    self.file_sha = compressed
  end

  def audit_sha
    AuditCsvFile.find_or_initialize_by(csv_file: self, date: Date.today).update!(sha: file_sha)
  end

  def compressed
    encoded = Base64.encode64(File.open(local_path, "rb").read)
    Digest::SHA1.hexdigest(encoded)
  end

  def drop_tables
    drop_table(fullmeasure_model_class.table_name) if table_exists?(self.class.table_name)
    drop_table(school_model_class.table_name) if table_exists?(self.class.school_table_name)
  end

  def import_service
    Csv::ImportService.new(csv_file: self)
  end

  def create_model_class(table_name)
    klass = Class.new(model_class_parent)
    klass.table_name = table_name
    klass
  end
end
