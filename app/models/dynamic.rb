class Dynamic < ApplicationRecord
  self.inheritance_column = nil
  self.primary_key = 'contact_id'

  scope :dirty, -> { where(dirty: true) }
end
