module Api
  class AudienceContactEnqueueService < EnqueueService
    protected

    def path
      @path ||= 'audience_students/'
    end

    def find_request
      @find_request ||= {}
      @find_request[model_instance.id] ||= query({ external_student_id: model_instance.contact_id, audience_id: model_instance.audience_id })
    end
  end
end