module Api
  class AudienceContactService < ApiService

    protected

    def csv_model_class
      AudienceContactCsv
    end

    def enqueue_service
      @enqueue_service ||= Api::AudienceContactEnqueueService
    end
  end
end
