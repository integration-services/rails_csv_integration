module Api
  class AudienceEnqueueService < EnqueueService
    protected

    def file_type
      @file_type ||= 'Audience'
    end

    def path
      if ENV['PLATFORM'] == 'beta'
        @path ||= 'audiences/audiences/'
      else
        @path ||= 'audiences/'
      end
    end

    def find_request
      @find_request ||= {}
      @find_request[model_instance.id] ||= query({ name: model_instance.name })
    end
  end
end