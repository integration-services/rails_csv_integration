module Api
  class AudienceService < ApiService

    def enqueue
      total = Audience.dirty.count
      i = 0

      Audience.dirty.update_all(last_attempt_to_push: Time.now)
      Audience.find_each do |audience|
        enqueue_service.new(model_instance: audience, hydra: hydra ).run
        i = i + 1
        puts "#{i}/#{total} complete"
        puts "#{((i.to_f/total.to_f) * 100).round(2)}% complete"
      end

      hydra.run
    end

    protected

    def csv_model_class
      Audience
    end

    def enqueue_service
      @enqueue_service ||= Api::AudienceEnqueueService
    end
  end
end
