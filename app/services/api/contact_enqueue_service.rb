module Api
  class ContactEnqueueService < EnqueueService

    protected

    def file_type
      @file_type ||= 'Contact'
    end


    def path
      if ENV['PLATFORM'] == 'beta'
        @path ||= 'contact-info/contacts/'
      else
        @path ||= 'students/'
      end
    end
  end
end