module Api
  class ContactPayloadService
    include ActiveAttr::Model
    attr_accessor :contact
    def payload
      @payload ||= normalized_attributes.merge!(custom_attributes)
    end


    def safe_attributes
      return @safe_attributes unless @safe_attributes.nil?
      @safe_attributes = contact.attributes

      @safe_attributes.map do |key, value|
        if key.include?('phone_number')
          @safe_attributes[key] = safe_phone_number(value)
        end
        if key.include?('email')
          @safe_attributes[key] = safe_email(value, contact.contact_id)
        end
        if key.include?('date') && important_attributes.include?(key)
          @safe_attributes[key] = safe_date(value)
        end
      end
      @safe_attributes['last_updated_from_csv_integration'] = DateTime.now
      @safe_attributes['source'] = 'csv_integration'
      @safe_attributes['do_not_text'] = !safe_boolean(contact.try(:can_text)) if contact.try(:can_text).present?
      @safe_attributes['do_not_text'] = safe_boolean(contact.try(:do_not_text)) if contact.try(:do_not_text).present?

      @safe_attributes['opted_in_to_texting'] = false
      @safe_attributes['opted_in_to_texting'] = !safe_boolean(contact.try(:did_not_opt_in_to_texting)) if contact.try(:did_not_opt_in_to_texting).present?
      @safe_attributes['opted_in_to_texting'] = safe_boolean(contact.try(:opted_in_to_texting)) if contact.try(:opted_in_to_texting).present?

      @safe_attributes['do_not_text'] = false if @safe_attributes['opted_in_to_texting'] == true && @safe_attributes['do_not_text'].nil?

      @safe_attributes['do_not_email'] = !safe_boolean(contact.try(:can_email)) if contact.try(:can_email).present?
      @safe_attributes['do_not_email'] = safe_boolean(contact.try(:do_not_email)) if contact.try(:do_not_email).present?
      @safe_attributes['first_name'] ||= safe_name(contact.try(:name), 'first', contact.contact_id) if contact.try(:name).present?
      @safe_attributes['last_name'] ||= safe_name(contact.try(:name), 'last', contact.contact_id) if contact.try(:name).present?

      @safe_attributes['integration_sha'] = contact.sha
      @safe_attributes['external_student_id'] = contact.contact_id
      @safe_attributes['type'] ||= 'InquiryStudent'
      @safe_attributes = @safe_attributes.except(*remove_attributes)
      @safe_attributes
    end

    def normalized_attributes
      safe_attributes.slice(*important_attributes)
    end

    def remove_attributes
      %w(
        id
        created_at
        updated_at
        last_successful_push
        last_attempt_to_push
        last_unsuccessful_push
        dirty
        status
        error_messages
        sha
        contact_id
        fullmeasure_id
      )
    end

    def important_attributes
      %w(
        first_name
        last_name
        email
        mobile_phone_number
        primary_phone_number
        date_of_birth
        external_student_id
        advisor_external_id
        program
        do_not_text
        opted_in_to_texting
        do_not_email
        integration_sha
        type
        record_type
        source
        last_updated_from_csv_integration
        first_inquiry_date
        inquiry_entry_term_name
        application_submitted_date
        application_entry_term_name
        application_stage
        application_decision_outcome
        is_registered
        high_school_name
        previous_college_name
      )
    end

    def custom_attributes
      { 'custom_attributes' => safe_attributes.except(*important_attributes).to_json }
    end

    def safe_phone_number(phone_number)
      return '5555550101' unless Rails.env.include? 'production'
      return '5555550101' if phone_number.blank?
      phone_number = phone_number.remove(/\D/)
      phone_number = phone_number.remove(/^1/) if phone_number.length > 10
      phone_number
    end

    def safe_email(email, contact_id)
      return "#{contact_id}@example.edu" unless Rails.env.include? 'production'
      return "#{contact_id}@example.edu" if email.blank?
      email
    end

    def safe_name(name, first_or_last, contact_id)
      if first_or_last == 'first'
        name.split(' ').first
      else
        last_name = name.split(' ').last
        return last_name unless last_name == ''
        contact_id
      end
    end

    def safe_boolean(boolean)
         return false if boolean.nil?
         if boolean == 1 || (boolean.is_a?(String) && (boolean.downcase.strip == 'true' || boolean.downcase.strip == 'yes' || boolean.downcase.strip == 'y' || boolean.strip == '1'))
           true
         else
           false
         end
       end

    def safe_date(date)
      return if date.nil? || date == 'NULL'
      date = date.gsub(' 00:00:00', '') if date.is_a?(String)

      date.to_date
    rescue
      if date.match(/\d{2}-\d{2}-\d{4}/).present?
        Date.strptime(date, '%m-%d-%Y')
      else
        Date.strptime(date, '%m/%d/%Y')
      end
    end
  end
end
