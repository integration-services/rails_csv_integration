module Api
  class ContactService < ApiService

    protected

    def csv_model_class
      ContactCsv
    end

    def enqueue_service
      @enqueue_service ||= Api::ContactEnqueueService
    end
  end
end