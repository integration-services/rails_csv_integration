module Api
  class EnqueueService < RequestService
    attr_accessor :model_instance, :hydra

    def run
      if model_instance.status == 'PENDING DELETE'
        enqueue_delete
      else
        enqueue_upsert
      end
    end

    protected

    def find_request
      @find_request ||= {}
      possible = IdMapping.where("contact_id_1 = ? OR contact_id_2 = ?", model_instance.contact_id, model_instance.contact_id)
      if possible.blank?
        @find_request[model_instance.contact_id] ||= query({ external_student_id: model_instance.contact_id })
      else
        all_possible = []
        possible.find_each do |p|
          all_possible << p.contact_id_1
          all_possible << p.contact_id_2
        end
        @find_request[model_instance.contact_id] ||= query({ external_student_id: all_possible })
      end
    end

    def enqueue_create
      complete create(model_instance.payload)
    end

    def enqueue_update
      complete update(model_instance.fullmeasure_id, model_instance.payload)
    end

    def enqueue_delete
      #complete delete(model_instance.fullmeasure_id)
      model_instance.update!(dirty: false)
    end

    def enqueue_upsert
      find_request.on_complete do |response|
        #possible = IdMapping.where("contact_id_1 = ? OR contact_id_2 = ?", model_instance.contact_id, model_instance.contact_id)
        #binding.pry unless possible.blank?
        if response.code == 200
          response_body = JSON.parse(response.body)
          if response_body.blank?

            enqueue_create
          else
            model_instance.update!(fullmeasure_id: response_body.first['id'])
            if response_body.length > 1 && file_type == 'Contact'
              response_body.each_with_index do |body, index|
                next if index == 0
                complete delete(body['id'])
              end
            end
            enqueue_update
          end
        else
          model_instance.update!(last_unsuccessful_push: Time.now, error_messages: "#{response.code}::#{response.body}")
        end
      end
      hydra.queue find_request
    end

    def complete(request)
      request.on_complete do |response|
        if response.code == 200
          if model_instance.status == 'PENDING DELETE'
            model_instance.destroy!
          else
            response_body = JSON.parse(response.body)
            status = "#{model_instance.status}D".remove('PENDING ')
            model_instance.update!(fullmeasure_id: response_body['id'], last_unsuccessful_push: nil, last_successful_push: Time.now, status: status, error_messages: nil, dirty: false)
          end
        else
          model_instance.update!(last_unsuccessful_push: Time.now, error_messages: "#{response.code}::#{response.body}")
        end
      end
      hydra.queue request
    end
  end
end