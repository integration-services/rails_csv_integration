module Api
  class IntegrationAuditService
    include ActiveAttr::Model
    attr_accessor  :debug, :localhost

    def upsert(payload)
      response = api_service.query(params: {})
      if response.blank?
        api_service.post(payload: payload)
      else
        api_service.put(id: response.first[:id], payload: payload)
      end
    end

    def api_service
      ActiveRecordApi::Request::Methods.new(
        host: host,
        path: 'api/web_services/csv_integration_audits',
        debug: debug,
        cache_store: Rails.cache,
        headers: {'Authorization' => "Token token=#{fme_auth_token}"}
      )
    end

    def host
      if Rails.env.downcase.starts_with?('production')
        ENV['PRODUCTION_FME_DOMAIN']
      else
        ENV['STAGE_FME_DOMAIN']
      end
    end

    def fme_auth_token
      if Rails.env.downcase.starts_with?('production')
        ENV['PRODUCTION_FME_AUTH_TOKEN']
      else
        ENV['STAGE_FME_AUTH_TOKEN']
      end
    end
  end
end
