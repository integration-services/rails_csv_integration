module Api
  class RequestService
    include ActiveAttr::Model
    attr_accessor :path, :debug

    def query(params = {})
      Typhoeus::Request.new(
        "#{domain}#{path}",
        method: :get,
        params: params,
        headers: headers,
        verbose: debug?
      )
    end

    def create(body)
      Typhoeus::Request.new(
        "#{domain}#{path}",
        method: :post,
        body: body,
        headers: headers,
        verbose: debug?
      )
    end

    def update(id, body)
      Typhoeus::Request.new(
        "#{domain}#{path}#{id}",
        method: :put,
        body: body,
        headers: headers,
        verbose: debug?
      )
    end

    def delete(id)
      Typhoeus::Request.new(
        "#{domain}#{path}#{id}",
        method: :delete,
        headers: headers,
        verbose: debug?
      )
    end

    def get(id)
      Typhoeus::Request.new(
        "#{domain}#{path}#{id}",
        method: :get,
        headers: headers,
        verbose: debug?
      )
    end

    def headers
      @headers ||= { Authorization: "Token token=#{fme_auth_token}", 'Content-Type' => 'application/x-www-form-urlencoded'}
    end

    def domain
      @domain ||= "#{fme_domain}/api/web_services/"
    end

    def fme_domain
      if Rails.env.downcase.starts_with?('production')
        ENV['PRODUCTION_FME_DOMAIN']
      else
        ENV['STAGE_FME_DOMAIN']
      end
    end

    def fme_auth_token
      if Rails.env.downcase.starts_with?('production')
        ENV['PRODUCTION_FME_AUTH_TOKEN']
      else
        ENV['STAGE_FME_AUTH_TOKEN']
      end
    end

    def path
      @path ||= 'students/'
    end

    def debug?
      return @debug unless @debug.nil?
      @debug = ENV['DEBUG'] || false
    end
  end
end