class ApiService

  def enqueue
    csv_model_class.find_each do |csv_file|
      batch_size = 20
      total = csv_file.fullmeasure_model_class.dirty.count
      i = 0
      puts csv_file.name
      csv_file.fullmeasure_model_class.dirty.update_all(last_attempt_to_push: Time.now)
      csv_file.fullmeasure_model_class.dirty.find_in_batches(batch_size: batch_size) do |model_instances|
        model_instances.each do |model_instance|
          enqueue_service.new(model_instance: model_instance, hydra: hydra ).run
        end
        i = i + batch_size
        puts "#{i}/#{total} complete"
        puts "#{((i.to_f/total.to_f) * 100).round(2)}% complete"
        hydra.run
        upload_db_to_s3 if (i % 5_000) == 0
      end
      upload_db_to_s3
    end
  end

  protected

  def upload_db_to_s3
    S3::DatabaseService.new.upload
  end

  def enqueue_service; end
  def csv_model_class; end

  def hydra
    @hydra ||= Typhoeus::Hydra.new(max_concurrency: 20)
  end
end
