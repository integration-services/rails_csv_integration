module Csv
  class AudienceContactService < Service

    protected

    def csv_file_model
      AudienceContactCsv
    end

    def ensure_removed_csv_tables_are_clean
      csv_file_model.where.not(full_measure_table_name: current_csv_table_names).soft_delete
    end

    def csv_prefix
      'audience'
    end
  end
end