require 'csv'
require 'iconv'
module Csv
  class CleaningService
    include ActiveAttr::Model
    attr_accessor :file_name

    def clean
      puts "CLEANING #{original_file_path.split('/').last}"
      time = Benchmark.realtime do
        File.delete(original_file_path) if File.zero?(original_file_path)
        return if File.zero?(original_file_path)
        return if file_name == 'fullmeasure_contact_id_mapping.csv'
        add_sha_to_csv
      end
      pp "Took #{time.real} seconds"
      puts "FINISHED CLEANING #{original_file_path.split('/').last}"
    end

    private

    def copy_to_path
      @copy_to_path ||= "#{local_path}/csv/#{clean_file_name}"
    end

    def clean_file_name
      return @clean_file_name unless @clean_file_name.nil?
      if file_name.starts_with?('fullmeasure_student')
        @clean_file_name ||= file_name_without_timestamp
      else
        @clean_file_name ||= "audience_#{file_name_without_timestamp}".gsub('audience_audience', 'audience')
      end
    end

    def original_file_path
      @original_file_path ||= "#{file_path}/#{file_name}"
    end

    def file_name_without_timestamp
      @file_name_without_timestamp ||= "#{file_name.downcase.remove(/_[^_][0-9]+\.csv$/).remove(/\.csv/)}.csv"
                                         .remove(/_\d{2}_\d{2}_\d{4}_\d{2}\.\d{2}\.\d{2}_a?A?p?P?M?m?/)
                                         .remove(/_\d{4}-\d{2}-\d{2}_\d{2}-\d{2}/)
                                         .remove(/_\d{4}-\d{2}-\d{2}/)
                                         .remove(/^fullmeasure_/)
                                         .gsub('-', '_')
                                         .gsub(' ', '_')
                                         .gsub('(', '_')
                                         .gsub(')', '_')
                                         .gsub(/[!~:]+/, '_' )
    end

    def file_path
      "#{local_path}/original_csv"
    end

    def add_sha_to_csv
      clean_file(nil)
    rescue
      clean_file('ISO-8859-1')
    end

    def clean_file(encoding = nil)
      encoding = (encoding.nil?) ? 'bom|utf-8' : "#{encoding}:utf-8"
      @contact_id_index = 0
      CSV.open(copy_to_path, 'wb', force_quotes: true, encoding: "UTF-8") do |csv_with_sha_row|
        CSV.foreach(original_file_path, encoding: encoding).each_with_index do |original_csv_row, index|
          puts "#{index} rows cleaned in #{original_file_path.split('/').last}" if (index % 5000) == 0
          if index == 0
            column_index = -1
            csv_with_sha_row << original_csv_row.map do |column_name|
              column_index = column_index + 1
              renamed_column = rename_column(column_name, column_index)
              @contact_id_index = column_index if renamed_column == 'contact_id'
              #binding.pry if copy_to_path.include?('student') && !copy_to_path.include?('audience')
              renamed_column
            end.push('sha').push('row_number')
          else
            column_index = -1
            row_number = index + 1
            csv_with_sha_row << original_csv_row.map do |value|
              column_index = column_index + 1
              new_value = remove_non_ascii(value)
              if column_index == @contact_id_index
                possible = IdMapping.where("contact_id_1 = ? OR contact_id_2 = ?", new_value, new_value) if id_mappings_exist?
                if possible.blank?
                  new_value
                else
                  if possible.where(source_1: %w(student primary)).present?
                    possible.where(source_1: %w(student primary)).first.contact_id_1
                  elsif possible.where(source_2: %w(student primary)).present?
                    possible.where(source_2: %w(student primary)).first.contact_id_2
                  else
                    possible.first.contact_id_2
                  end
                end
              end
              new_value
            end.push(compressed(original_csv_row)).push(row_number)
          end
          next unless (index%50) == 0
          GC.start
          csv_with_sha_row.flush
        end
      end
    end

    def id_mappings_exist?
      return @id_mappings_exist unless @id_mappings_exist.nil?
      @id_mappings_exist = IdMapping.count > 0
    end

    def rename_column(column_name, column_index)
      return "header_missing_#{column_index}" if column_name.nil? || column_name.try(:strip) == ''
      column_name = remove_non_ascii(column_name || '')
      return column_name unless File.exist?(mapping_csv)
      new_column_name = mapping.detect { |row_hash| row_hash['school_name'] == column_name }
      return column_name if new_column_name.nil?
      new_column_name['fullmeasure_name']
    end

    def remove_non_ascii(string)
      string = Iconv.conv('ASCII//IGNORE', 'UTF8', string)
      string.strip.gsub(/[\u0080-\u00ff]/,'?').gsub(/[^\x00-\x7F]/, '?')
    end

    def mapping
      @mapping ||= CSV.foreach(mapping_csv, headers: true).map(&:to_h)
    end

    def mapping_csv
      "#{local_path}/attribute_mapping.csv"
    end

    def compressed(csv_row)
      Digest::SHA1.hexdigest(csv_row.to_json)
    end

    def local_path
      ENV['PROJECT_DIR']
    end
  end
end
