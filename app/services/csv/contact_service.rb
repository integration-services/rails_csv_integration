module Csv
  class ContactService < Service

    protected

    def csv_file_model
      ContactCsv
    end

    def ensure_removed_csv_tables_are_clean
      csv_file_model.where.not(full_measure_table_name: current_csv_table_names).destroy_all
    end

    def csv_prefix
      'student'
    end
  end
end