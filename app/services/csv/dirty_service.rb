module Csv
  class DirtyService
    include ActiveAttr::Model
    attr_accessor :csv_file

    delegate :fullmeasure_table_name, :fullmeasure_model_class, :school_model_class, :school_table_name, to: :csv_file

    def mark_and_persist_records
      puts 'remove bad data'
      remove_bad_data if csv_file.is_a? ContactCsv
      puts 'mark needs created'
      mark_needs_created if csv_file.is_a? ContactCsv
      puts 'mark needs deleted'
      mark_needs_deleted if csv_file.is_a? ContactCsv
      puts 'mark needs updated'
      mark_needs_updated if csv_file.is_a? ContactCsv
    end

    protected

    def remove_bad_data
      fullmeasure_model_class.where(status: 'PENDING CREATE', last_successful_push: nil).where.not(last_unsuccessful_push: nil).destroy_all
    end

    def mark_needs_created
      @create_time = 0
      # Raw sql is so much faster than any insert active record can do. Believe me I tried @samuelbirk
      ActiveRecord::Base.connection.execute(
        <<-SQL
          INSERT INTO #{fullmeasure_table_name} (contact_id) 
            SELECT contact_id FROM #{school_table_name} 
              WHERE contact_id NOT IN(SELECT contact_id FROM #{fullmeasure_table_name})
        SQL
      )
      batch_size = 20
      total = fullmeasure_model_class.where(status: nil).count
      i = 0
      fullmeasure_model_class.where(status: nil).find_in_batches(batch_size: 20) do |fullmeasure_rows|
        @create_time += Benchmark.realtime do
          fullmeasure_rows.each do |fullmeasure_row|
            school_row = school_model_class.find_by_contact_id(fullmeasure_row.contact_id)
            updates = school_model_class.column_names.flat_map { |column_name| {"#{column_name}" => "#{school_row.send(column_name)}"} }.reduce({}, :merge)
            updates = updates.merge('status' => 'PENDING CREATE', 'dirty' => 1, 'updated_at' => Time.now.to_s(:db), 'created_at' => Time.now.to_s(:db))
            fullmeasure_model_class.find_by(contact_id: school_row.contact_id).update!(updates)
          end
        end
        i = i + batch_size
        next unless  (i % 500) == 0
        puts "#{i}/#{total} of #{csv_file} marked pending create in database in #{@create_time} seconds"
        puts "#{((i.to_f/total.to_f) * 100).round(2)}% marked pending in database"
        upload_db_to_s3 if (i % 500) == 0
      end
    end

    def mark_needs_deleted
      fullmeasure_model_class.where.not(contact_id: school_model_class.select(:contact_id)).update_all(status: 'PENDING DELETE', dirty: true, updated_at: Time.now)
    end

    def mark_needs_updated
      batch_size = 20
      total = school_model_class.where.not(sha: fullmeasure_model_class.select(:sha)).count
      i = 0
      @update_time = 0

      school_model_class.where.not(sha: fullmeasure_model_class.select(:sha)).find_in_batches(batch_size: 20) do |school_rows|
        @update_time += Benchmark.realtime do

          school_rows.each do |school_row|
            updates = school_model_class.column_names.flat_map { |column_name| {"#{column_name}" => "#{school_row.send(column_name)}"} }.reduce({}, :merge)
            updates = updates.merge('status' => 'PENDING UPDATE', 'dirty' => 1, 'updated_at' => Time.now.to_s(:db))
            fullmeasure_model_class.find_by(contact_id: school_row.contact_id).update!(updates)
          end
        end
        i = i + batch_size
        next unless  (i % 500) == 0
        puts "#{i}/#{total} of #{csv_file}  marked pending update in database in #{@update_time} seconds"
        puts "#{((i.to_f/total.to_f) * 100).round(2)}% marked pending in database"
        upload_db_to_s3 if (i % 500) == 0
      end
    end

    def upload_db_to_s3
      S3::DatabaseService.new.upload
    end
  end
end
