module Csv
  class ImportService
    include ActiveAttr::Model
    attr_accessor :csv_file

    def upsert_tables
      create_school_table
      upsert_fullmeasure_table
    end

    protected

    delegate :name, :header_sha, :fullmeasure_table_name, :school_table_name, :fullmeasure_model_class, :school_model_class, to: :csv_file
    delegate :connection, to: ActiveRecord::Base
    delegate :table_exists?, to: :connection
    delegate :remove_column, :drop_table, :change_table, :add_column, :add_timestamps, :column_exists?, to: ActiveRecord::Migration

    def create_school_table
      drop_table(school_table_name) if table_exists?(school_table_name)
      import_to_table(school_table_name)
    end


    def upsert_fullmeasure_table
      import_to_table(fullmeasure_table_name) unless table_exists?(fullmeasure_table_name)
      add_columns
      remove_columns
    end

    def columns_to_remove
      fullmeasure_model_class.column_names - valid_column_names
    end

    def remove_columns
      columns_to_remove.each do |column_name|
        column_name = column_name.remove(/"/).to_sym
        remove_column(fullmeasure_table_name, column_name) if column_exists?(fullmeasure_table_name, column_name)
      end
    end

    def add_columns
      columns_to_add.each do |column|
        unless column_exists?(fullmeasure_table_name, column.name)
          add_column(fullmeasure_table_name, column.name, column.type, default: nil)
        end
      end

      school_model_class.column_names.each do |column_name|
        column_name =  column_name.remove(/"/).to_sym
        unless column_exists?(fullmeasure_table_name, column_name)
          add_column(fullmeasure_table_name, column_name, :string, default: nil)
        end
      end
    end

    def valid_column_names
      columns_to_add.map(&:name).map(&:to_s) +  school_model_class.column_names
    end

    def columns_to_add
      return @columns_to_add unless @columns_to_add.nil?
      @columns_to_add = [
          OpenStruct.new(name: :status, type: :string, index: true),
          OpenStruct.new(name: :fullmeasure_id, type: :integer),
          OpenStruct.new(name: :dirty, type: :boolean, index: true),
          OpenStruct.new(name: :last_attempt_to_push, type: :datetime),
          OpenStruct.new(name: :last_successful_push, type: :datetime),
          OpenStruct.new(name: :last_unsuccessful_push, type: :datetime),
          OpenStruct.new(name: :error_messages, type: :text),
          OpenStruct.new(name: :id, type: :primary_key),
          OpenStruct.new(name: :created_at, type: :datetime),
          OpenStruct.new(name: :updated_at, type: :datetime)
      ]
      @columns_to_add << OpenStruct.new(name: :audience_id, type: :integer) if csv_file.is_a? AudienceContactCsv
      @columns_to_add
    end


    def import_to_table(table_name)
      result = system "sqlite3 db/#{Rails.env}.sqlite3 \".mode csv\" \".import #{csv_file.local_path} #{table_name}\""
      #raise "DATABASE LOAD ERROR:: #{result}" unless result
      table_exists?(table_name) #needs to run to reset the table cache otherwise Rails will complain that the table does not exist yet
    end

    def compressed(row)
      Digest::SHA1.hexdigest(row.to_json)
    end
  end
end
