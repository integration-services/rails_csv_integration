module Csv
  class Service
    def upsert
      cleanup_csv_tables
      csv_file_model.find_each do |csv_file|
        puts "upserting #{csv_file.local_path}"
        next unless File.exists?(csv_file.local_path)
        DirtyService.new(csv_file: csv_file).mark_and_persist_records
        S3::CsvService.new(csv_file: csv_file).upload
      end
    end

    protected

    def cleanup_csv_tables
      ensure_csv_tables_exist_in_database
      ensure_removed_csv_tables_are_destroyed
    end

    def csv_file_model; end
    def csv_prefix; end

    def ensure_removed_csv_tables_are_destroyed; end

    def ensure_csv_tables_exist_in_database
      current_csvs.each do |file|
        csv_file = csv_file_model.find_or_create_by!(name: File.basename(file))
        csv_file.update!(header_sha: compressed(File.open(csv_file.local_path, &:readline)))
      end
    end

    def current_csv_table_names
      current_csvs.map { |file| full_measure_table_name(file) }
    end

    def current_tables
      ActiveRecord::Base.connection.tables.select { |table_name| table_name.starts_with?(csv_prefix) }
    end

    def current_csvs
      Dir["#{local_path}/csv/#{csv_prefix}*.csv"]
    end

    def local_path
      ENV['PROJECT_DIR']
    end

    def compressed(row)
      Digest::SHA1.hexdigest(row.to_json)
    end
  end
end
