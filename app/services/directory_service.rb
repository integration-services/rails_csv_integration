require 'fileutils'

class DirectoryService
  include ActiveAttr::Model

  def create
    FileUtils::mkdir_p "#{local_path}/csv"
    FileUtils::mkdir_p "#{local_path}/writeback"
    FileUtils::mkdir_p "#{local_path}/original_csv"
    FileUtils::mkdir_p "#{local_path}/log"
    FileUtils::mkdir_p "#{local_path}/errors"
  end

  def local_path
    ENV['PROJECT_DIR']
  end
end

