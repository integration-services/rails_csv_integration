require 'fileutils'
class EncryptService
  include ActiveAttr::Model

  def encrypt
    `zip --password #{encryption_key} #{ENV['PROJECT_DIR']}/#{ENV['CI_PROJECT_NAME']}.zip #{ENV['PROJECT_DIR']}/**/*.csv`
  end

  def encryption_key
    if Rails.env.production?
      ENV['PRODUCTION_FME_AUTH_TOKEN']
    else
      ENV['STAGE_FME_AUTH_TOKEN']
    end
  end
end

