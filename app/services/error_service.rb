require 'csv'
require 'fileutils'

class ErrorService
  include ActiveAttr::Model
  attr_accessor :debug

  def create
    download_audience_errors
    create_contact_error_files
    write_stale_errors_to_file
    delete_empty_error_files
  end

  def write_api_errors_to_file(csv_file)
    CSV.open("#{local_path}/errors/#{csv_file.name}", 'w+') do |csv_row|
      next if csv_file.fullmeasure_model_class.first.nil?
      csv_row << csv_file.fullmeasure_model_class.first.attributes.keys
      csv_file.fullmeasure_model_class.dirty.where.not(error_messages: nil).find_in_batches(batch_size: 500) do |students|
        students.each do |student|
          csv_row << student.attributes.values
        end
      end
    end
  end

  def write_stale_errors_to_file
    CSV.open("#{local_path}/errors/stale.csv", 'w+') do |csv_row|
      csv_row << %w(name last_refreshed)
      CsvFile.find_each do |csv_file|
        next if csv_file.file_sha.nil?
        next unless csv_file.file_sha == AuditCsvFile.find_by(csv_file: csv_file, date: 5.days.ago.to_date).try(:sha)
        csv_row << [csv_file.name, AuditCsvFile.where(csv_file: csv_file, sha: csv_file.file_sha).order(:date).last.date]
      end
    end
  end

  def create_contact_error_files
    ContactCsv.find_each do |csv_file|
      write_api_errors_to_file(csv_file)
    end
  end

  def download_audience_errors
    AudienceContactCsv.find_each do |csv_file|
      S3::CsvErrorService.new(csv_file: csv_file).download
    end
  end

  def delete_empty_error_files
    Dir.glob("#{local_path}/errors/*.csv") do |file|
      next unless File.exist?(file)
      File.delete(file) if File.open(file).readlines.size == 1
    end
  end

  def local_path
    ENV['PROJECT_DIR']
  end
end

