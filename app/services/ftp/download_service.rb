require 'net/ssh/proxy/command'
require 'fileutils'
require 'csv'
module Ftp
  class DownloadService
    include ActiveAttr::Model
    attr_accessor :debug

    def download
      delete_csvs if Rails.env.development?
      puts 'CLEANING LIST'
      cleaned_list.each do |file|
        Api::IntegrationFileService.new(debug: true).upsert(file)
      end
      latest_files.each do |file|
        puts "Downloading #{file}"
        Service.new(
          file_path: file[:path],
          file_name: file[:name],
          local_path: local_path,
          debug: debug
        ).download
      end
    end

    def clean
      latest_files.each do |file|
        Csv::CleaningService.new(file_name: file[:name]).clean
      end
    end

    #private
    #

    def delete_csvs
      Dir.glob("#{local_path}/**/*.csv").each do |file|
        next if file.ends_with?('attribute_mapping.csv') || file.ends_with?('blacklist.csv')
        File.delete(file) if File.exists?(file)
      end
    end

    def latest_files
      @latest_files ||= file_names_without_timestamps.map { |file_name| sorted_files.detect { |file| file[:name_without_timestamp] == file_name } }.reject do |file|
        blacklist.include?(file[:name])
      end
    end

    def blacklist
      return [] unless File.exists?(blacklist_csv)
      @blacklist ||= CSV.foreach(blacklist_csv, headers: false).map(&:first)
    end

    def blacklist_csv
      "#{local_path.remove('/original_csv')}blacklist.csv"
    end

    def file_names_without_timestamps
      @file_names ||= sorted_files.map { |file| file[:name_without_timestamp] }.compact.uniq
    end

    def cleaned_list
      @cleaned_list ||= latest_files.map do |item|
        {
          filename: item[:name],
          cleaned_name: item[:name_without_timestamp],
          last_modified_at: Time.at(item[:last_modified_at]).to_datetime,
        }
      end
    end

    def sorted_files
      return @sorted_files unless @sorted_files.nil?
      @sorted_files = []
      paths = ENV['FTP_PATH'].split(',')
      paths.each do |path|
        path = path.remove(/"/)
        path.strip!
        @sorted_files += Service.new(file_path: path, file_name: '*.csv', debug: debug).list_files
      end
      @sorted_files = @sorted_files.sort{|a,z| z[:last_modified_at]<=>a[:last_modified_at] }
    end

    def local_path
      "#{ENV['PROJECT_DIR']}/original_csv/"
    end
  end
end

