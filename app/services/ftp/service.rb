require 'net/ssh/proxy/command'
module Ftp
  class Service
    include ActiveAttr::Model
    attr_accessor :file_path, :file_name, :local_path, :debug

    def download
      last_modified = sftp.dir.glob(file_path, file_name).first&.attributes.mtime.to_s
      puts "DOWNLOADING:: #{file_name} with LAST MODIFIED: #{last_modified} "
      sftp.download!("#{file_path}/#{file_name}", "#{local_path}#{file_name}")  do |event, _downloader, *args|
        metadata, byte_offset, received_data = args
        case event
        when :open then
          puts "starting download: #{metadata.remote} -> #{metadata.local} (#{metadata.size} bytes}" if debug?
        when :get then
          puts "writing #{received_data.length} bytes to #{metadata.local} starting at #{byte_offset}" if debug?
        when :close then
          puts "finished with #{metadata.remote}" if debug?
        when :mkdir then
          puts "creating directory #{metadata}" if debug?
        when :finish then
          puts "all done!" if debug?
        end
        rerun_externally_modified(last_modified)
      end
    rescue => exception
      Honeybadger.notify(exception, context: {model: self.class.name, attributes: self.attributes, file_name: "#{file_path}/#{file_name}", intg: ENV['CI_PROJECT_NAME']})
    end

    def upload_dir
      puts "UPLOADING:: #{local_path}"
      sftp.upload!(local_path, file_path)
    end

    def list_files
      sftp.dir.glob(file_path, file_name).map do |file|
        {
          path: file_path,
          name: file.name,
          last_modified_at: file.attributes.mtime,
          name_without_timestamp: remove_timestamp(file.name)
        }
      end
    end

    def delete_files
      # ignores not found
      sftp.remove("#{file_path}/#{file_name}")
    end

    protected

    def sftp
      if ENV['FTP_PASSWORD'].present?
        @sftp ||= Net::SFTP.start(ENV['FTP_DOMAIN'], ENV['FTP_USERNAME'], password:  ENV['FTP_PASSWORD'], verbose: verbose_logger, verify_host_key: :never)
      elsif ENV['FTP_AUTH_KEY'].present?
        #proxy = Net::SSH::Proxy::Command.new("ssh -o StrictHostKeyChecking=no -W %h:%p ubuntu@#{ENV['CLIENT_BASTION']}")
        #@sftp ||= Net::SFTP.start(ENV['FTP_DOMAIN'], ENV['FTP_USERNAME'], key_data: [ ENV['FTP_AUTH_KEY'] ], verbose: verbose_logger, proxy: proxy, verify_host_key: :never)
        @sftp ||= Net::SFTP.start(ENV['FTP_DOMAIN'], ENV['FTP_USERNAME'], keys: [ '/current/sftp_key' ], verbose: verbose_logger, verify_host_key: :never)
      end
    end

    def remove_timestamp(file_name)
      file_name.remove(/\.csv_\d{4}-\d{2}-\d{2}_\d{2}-\d{2}\.csv/).remove(/_[^_][0-9]+\.csv$/).remove(/\.csv/).remove(/_\d{2}_\d{2}_\d{4}_\d{2}\.\d{2}\.\d{2}_A?P?M/).remove(/_\d{4}-\d{2}-\d{2}/).remove(/\d{2}-\d{2}/).remove(/\d{2}_\d{2}/)
    end

    def verbose_logger
      return Logger::ERROR unless debug?
      Logger::DEBUG
    end

    def rerun_externally_modified(last_modified_before)
      sftp.dir.glob(file_path, file_name).each do |file|
        download if file.attributes.mtime.to_s != last_modified_before
      end
    end

    def debug?
      return @debug unless @debug.nil?
      @debug = ENV['DEBUG'] || false
    end
  end
end
