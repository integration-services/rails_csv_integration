module Ftp
  class WritebackService
    include ActiveAttr::Model
    attr_accessor :debug

    def upload
      delete_csvs
      Service.new(file_path: ENV['FTP_WRITEBACK_PATH'], local_path: "#{local_path}/writeback", debug: debug).upload_dir
    end

    private

    def delete_csvs
      Service.new(file_path: ENV['FTP_WRITEBACK_PATH'], file_name: '*.csv', debug: debug).delete_files
    end

    def local_path
      ENV['PROJECT_DIR']
    end
  end
end

