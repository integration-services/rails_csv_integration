require 'net/ssh/proxy/command'
class IntegrationService
  include ActiveAttr::Model
  attr_accessor :debug
  delegate :connection, to: ActiveRecord::Base
  delegate :drop_table, :table_exists?, :execute, to: :connection
  def run
    Api::IntegrationAuditService.new(debug: true).upsert( {last_attempt: DateTime.now})
    create_directory_structure
    download_db
    writeback_csvs if Rails.env.production?
    download_csvs
    import_contact_id_mapping_to_table
    clean_csvs
    upsert_csvs_to_db
    upsert_csvs_to_api
    upload_db
    create_error_files
    email_error_files
    Api::IntegrationAuditService.new(debug: true).upsert( {last_success: DateTime.now})  end

  def create_directory_structure
    puts 'create directory structure'
    DirectoryService.new.create
  end

  def download_db
    puts 'downloading and migrating database'
    S3::DatabaseService.new.download unless Rails.env.development?
    system('bundle exec rake db:create')
    system('bundle exec rake db:migrate')
  end

  def download_csvs
    puts 'downloading csvs from remote source'
    Ftp::DownloadService.new.download if ENV['FTP_TYPE'].downcase.include? 'ftp'
    S3::DownloadService.new.download if ENV['FTP_TYPE'].downcase.include? 's3'
  end

  def clean_csvs
    puts 'cleaning csvs from remote source'
    Ftp::DownloadService.new.clean if ENV['FTP_TYPE'].downcase.include? 'ftp'
    S3::DownloadService.new.clean if ENV['FTP_TYPE'].downcase.include? 's3'
  end

  def upsert_csvs_to_db
    puts 'upserting csvs into database'
    Csv::ContactService.new.upsert
    Csv::AudienceContactService.new.upsert
  end

  def upsert_csvs_to_api
    puts 'upserting data into ice via web services apis'
    Api::ContactService.new.enqueue
    Api::AudienceService.new.enqueue
  end

  def upload_db
    puts 'upload database to s3'
    S3::DatabaseService.new.upload
  end

  def create_error_files
    puts 'archiving data errors'
    ErrorService.new.create
    EncryptService.new.encrypt
  end

  def email_error_files
    puts 'emailing data errors'
    ErrorMailer.error(to: 'dev@fullmeasureed.com', attachment: "#{ENV['PROJECT_DIR']}/#{ENV['CI_PROJECT_NAME']}.zip").deliver_now!
    return if ENV['FTP_ADMIN_EMAIL'].nil?
    ErrorMailer.error(to: ENV['FTP_ADMIN_EMAIL'], attachment: "#{ENV['PROJECT_DIR']}/#{ENV['CI_PROJECT_NAME']}.zip").deliver_now!
  end


  def import_contact_id_mapping_to_table
    puts "#{local_path}/original_csv/fullmeasure_contact_id_mapping.csv"
    puts File.exists?("#{local_path}/original_csv/fullmeasure_contact_id_mapping.csv")
    return unless File.exists?("#{local_path}/original_csv/fullmeasure_contact_id_mapping.csv")
    drop_table('temp_id_mappings') if table_exists?('temp_id_mappings')
    system "sqlite3 db/#{Rails.env}.sqlite3 \".mode csv\" \".import #{local_path}/original_csv/fullmeasure_contact_id_mapping.csv temp_id_mappings\""
    table_exists?('temp_id_mappings') #needs to run to reset the table cache otherwise Rails will complain that the table does not exist yet
    execute("DELETE FROM id_mappings")
    execute("INSERT INTO id_mappings (contact_id_1, contact_id_2, source_1, source_2, updated_at, created_at) SELECT contact_id_1, contact_id_2, source_1, source_2, datetime('now'), datetime('now') FROM temp_id_mappings")
  end

  def local_path
    ENV['PROJECT_DIR']
  end

  def writeback_csvs
    puts 'Placing writebacks in remote source'
    return unless  ENV['FTP_WRITEBACK_PATH'].present? ||  ENV['S3_WRITEBACK_PATH'].present?
    Writeback::Service.new(debug: debug, params: {updated_at: 4.hours.ago.to_datetime.strftime("%Y-%m-%d %H:%M:%S")}).run
  end
end

