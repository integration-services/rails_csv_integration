module S3
  class CsvErrorService < Fme::Service
    attr_accessor :csv_file
    delegate :s3_path, :local_path, to: :csv_file

    def local_path
      @local_path ||= csv_file.local_path.gsub('/csv', '/errors').gsub('.csv', '.errors.csv')
    end

    def s3_path
      @s3_path ||= csv_file.s3_path.gsub("/#{ENV['CI_COMMIT_REF_NAME']}/", "/#{ENV['CI_COMMIT_REF_NAME']}/errors/").gsub('.csv', '.errors.csv')
    end
  end
end
