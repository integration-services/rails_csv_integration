module S3
  class CsvService < Fme::Service
    attr_accessor :csv_file
    delegate :s3_path, :local_path, to: :csv_file
  end
end
