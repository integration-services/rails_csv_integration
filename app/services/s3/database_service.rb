module S3
  class DatabaseService < Fme::Service
    def s3_path
      "#{super}/db/#{Rails.env}.sqlite3"
    end

    def local_path
     "#{Rails.application.config.root}#{local_path_extension}/db/#{Rails.env}.sqlite3"
    end
  end
end