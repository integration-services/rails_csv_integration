require 'csv'
module S3
  class DownloadService
    include ActiveAttr::Model
    attr_accessor :debug

    def download
      delete_csvs if Rails.env.development?
      cleaned_list.each do |file|
        Api::IntegrationFileService.new(debug: true).upsert(file)
      end
      latest_files.each do |file|
        School::Service.new(s3_path: file[:name], local_path_extension: "/original_csv/#{file[:name].split('/')[-1]}").download
        Csv::CleaningService.new(file_name: File.basename(file[:name])).clean
      end
    end

    def clean
      latest_files.each do |file|
        Csv::CleaningService.new(file_name: File.basename(file[:name])).clean
      end
    end

    private

    def delete_csvs
      Dir.glob("#{ENV['PROJECT_DIR']}/**/*.csv").each do |file|
        next if file.ends_with?('attribute_mapping.csv') || file.ends_with?('blacklist.csv')
        File.delete(file) if File.exists?(file)
      end
    end

    def delete_writebacks
      Dir.glob("#{ENV['PROJECT_DIR']}/writeback/**/*.csv").each do |file|
        File.delete(file) if File.exists?(file)
      end
    end

    def cleaned_list
      @cleaned_list ||= latest_files.map do |item|
        filename = item[:name].split('/').last
        cleaned_filename = item[:name_without_timestamp].split('/').last
        {
          filename: filename,
          cleaned_name: cleaned_filename,
          last_modified_at: item[:last_modified_at].to_datetime,
        }
      end
    end

    def latest_files
      @latest_files ||= file_names_without_timestamps.map { |file_name| sorted_files.detect { |file| file[:name_without_timestamp] == file_name } }.reject do |file|
        blacklist.include?(file[:name])
      end
    end

    def blacklist
      return [] unless File.exists?(blacklist_csv)
      @blacklist ||= CSV.foreach(blacklist_csv, headers: false).map(&:first)
    end

    def blacklist_csv
      "#{ENV['PROJECT_DIR']}/blacklist.csv"
    end

    def file_names_without_timestamps
      @file_names ||= sorted_files.map { |file| file[:name_without_timestamp] }.compact.uniq
    end

    def sorted_files
      @sorted_files ||= School::Service.new.list_objects.sort{|a,z| z[:last_modified_at]<=>a[:last_modified_at] }.reverse
    end

    def debug?
      return @debug unless @debug.nil?
      @debug = ENV['DEBUG'] || false
    end
  end
end

