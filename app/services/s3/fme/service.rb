module S3
  module Fme
    class Service < ::S3::Service
      include ActiveAttr::Model
      attr_accessor :s3_path_extension

      protected

      def region
        @region ||= ENV['AWS_DEFAULT_REGION']
      end

      def access_key_id
        @access_key_id ||= ENV['AWS_ACCESS_KEY_ID']
      end

      def secret_access_key
        @secret_access_key ||= ENV['AWS_SECRET_ACCESS_KEY']
      end

      def s3_path
        @s3_path ||= "#{ENV['CI_PROJECT_NAME']}/#{ENV['CI_COMMIT_REF_NAME']}#{s3_path_extension}"
      end

      def bucket
        @bucket ||= 'fme-integration-api'
      end
    end
  end
end
