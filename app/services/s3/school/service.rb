module S3
  module School
    class Service < ::S3::Service
      include ActiveAttr::Model
      attr_accessor :s3_path_extension

      protected

      def region
        @region ||= ENV['S3_REGION']
      end

      def access_key_id
        @access_key_id ||= ENV['S3_ACCESS_KEY_ID']
      end

      def secret_access_key
        @secret_access_key ||= ENV['S3_SECRET_ACCESS_KEY']
      end

      def bucket
        @bucket ||= ENV['S3_BUCKET']
      end

      def s3_path
        @s3_path ||= "#{ENV['S3_PATH']}#{s3_path_extension}"
      end
    end
  end
end
