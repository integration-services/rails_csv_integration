require 'fileutils'
require 'aws-sdk-s3'
module S3
  class Service
    include ActiveAttr::Model
    attr_accessor :bucket, :s3_path, :region, :access_key_id, :secret_access_key, :local_path, :local_path_extension

    def upload
      object.upload_file(local_path)
    end

    def upload_dir
      Dir.glob("#{local_path}/**/*.csv").each do |filename|
        puts "UPLOADING: #{filename} to S3:#{bucket}/#{s3_path}/#{short_name(filename)}"
        s3.bucket(bucket).object("#{s3_path}/#{short_name(filename)}").upload_file(filename)
      end
    end

    def download
      last_modified = object.last_modified.to_s
      puts "DOWNLOADING:: #{s3_path} with LAST MODIFIED: #{last_modified} "
      puts "Response Target: #{local_path}"
      object.get(response_target: "#{local_path}")
      rerun_externally_modified(last_modified)
    rescue Aws::S3::Errors::NotFound => e
      Rails.logger.error e
    end

    def download_all
      s3.bucket(bucket).objects(prefix: s3_path).each do |obj|
        next unless obj.key.end_with?('.csv')
        obj.get(response_target: "#{local_path}/#{short_name(obj.key)}")
      end
    end

    def delete_all
      s3.bucket(bucket).objects(prefix: s3_path).each do |object|
        object.delete
      end
    end

    def list_objects
      s3.bucket(bucket).objects(prefix: s3_path).flat_map do |object|
        next unless object.key.include?('.csv')
        {
          name: object.key,
          last_modified_at: object.last_modified.to_s,
          name_without_timestamp: remove_timestamp(object.key)
        }
      end.compact
    end

    protected

    def object
      s3.bucket(bucket).object(s3_path)
    end

    def s3
      @s3 ||= Aws::S3::Resource.new(
        region: region,
        access_key_id: access_key_id,
        secret_access_key: secret_access_key
      )
    end

    def local_path
      @local_path ||= "#{ENV['PROJECT_DIR']}#{local_path_extension}"
    end

    private

    def short_name(filename)
      filename.split('/')[-1]
    end

    def remove_timestamp(file_name)
      file_name.remove(/\.csv_\d{4}-\d{2}-\d{2}_\d{2}-\d{2}\.csv/).remove(/_[^_][0-9]+\.csv$/).remove(/\.csv/).remove(/_\d{2}_\d{2}_\d{4}_\d{2}\.\d{2}\.\d{2}_A?P?M/).remove(/_\d{4}-\d{2}-\d{2}/).remove(/\d{2}-\d{2}/).remove(/\d{2}_\d{2}/)
    end

    def rerun_externally_modified(last_modified_before)
      download if object.last_modified.to_s != last_modified_before
    end
  end
end
