module S3
  class WritebackService
    include ActiveAttr::Model
    attr_accessor :debug

    def download_writebacks
      delete_local_csvs
      Fme::Service.new(s3_path_extension: '/writeback', local_path: local_path).download_all
    end

    def upload
      #delete_remote_csvs
      School::Service.new(s3_path: ENV['S3_WRITEBACK_PATH'], local_path: local_path).upload_dir
    end

    private

    def delete_local_csvs
      Dir.glob("#{local_path}/**/*.csv").each do |file|
        File.delete(file) if File.exists?(file)
      end
    end

    def delete_remote_csvs
      School::Service.new(s3_path: ENV['S3_WRITEBACK_PATH'], file_name: '*.csv', debug: debug).delete_all
    end

    def local_path
      "#{ENV['PROJECT_DIR']}/writeback"
    end

    def debug?
      return @debug unless @debug.nil?
      @debug = ENV['DEBUG'] || false
    end
  end
end

