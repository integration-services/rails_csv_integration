module Writeback
  class ApiService
    include ActiveAttr::Model
    attr_accessor :path, :debug, :domain, :response_format

    def get(params = {})
      symbolize_response(authenticated_connection.get(url, all_params(params)).body)
    rescue => exception
      raise
    end

    def post(payload, params = {})
      symbolize_response(authenticated_connection.post(url_with_params(params), payload).body)
    rescue => exception
      raise
    end

    def url_with_params(params = {})
      "#{url}?#{all_params(params).to_query}"
    end

    def url
      "#{strip_trailing_slash(domain)}/#{strip_trailing_slash(path)}"
    end

    def authentication_params
      {}
    end

    def all_params(params)
      authentication_params.merge(params)
    end

    def strip_trailing_slash(string)
      string.sub(%r{(\/)+$}, '')
    end

    def symbolize_response(response)
      return response if response.is_a? String
      if response.is_a? Array
        return response if response.first.is_a? Array
        response.map!(&:deep_symbolize_keys!)
      else
        response.try(:deep_symbolize_keys!)
      end
    end

    def response_format
      @response_format ||= 'json'
    end

    def authenticated_connection
      connection.headers = connection.headers.merge({ Authorization: "Token token=#{fme_auth_token}", 'Content-Type' => 'application/x-www-form-urlencoded'})
      connection
    end

    def domain
      if Rails.env.downcase.starts_with?('production')
        "#{ENV['PRODUCTION_FME_DOMAIN']}/api/web_services/"
      else
        "#{ENV['STAGE_FME_DOMAIN']}/api/web_services/"
      end
    end

    def fme_auth_token
      if Rails.env.downcase.starts_with?('production')
        ENV['PRODUCTION_FME_AUTH_TOKEN']
      else
        ENV['STAGE_FME_AUTH_TOKEN']
      end
    end

    def connection
      @connection ||= Faraday.new ssl: { verify: false } do |builder|
        builder.request :json
        builder.request :url_encoded
        builder.response :follow_redirects, limit: 3
        builder.response :raise_error
        builder.response response_format.to_sym, content_type: /\b#{response_format}/
        builder.response :logger if debug
        builder.adapter Faraday.default_adapter
      end
    end
  end
end
