require 'csv'
module Writeback
  class Service
    include ActiveAttr::Model
    attr_accessor :debug, :params

    def run
      download
      upload
    end

    def download
      %w(student_writebacks message_writebacks).each do |path|
        CSV.open("#{local_path}/#{filename(path)}", 'wb', force_quotes: true, encoding: "UTF-8") do |csv|
          results = ApiService.new(debug: debug, path: path).get(params)
          return unless results.length > 0
          csv << results.first.keys
          results.each do |row|
            csv << row.values
          end
        end
      end
    end

    def upload
      Ftp::WritebackService.new(debug: debug).upload if ENV['FTP_TYPE'].downcase.include?('ftp') && ENV['FTP_WRITEBACK_PATH'].present?
      S3::WritebackService.new(debug: debug).upload if ENV['FTP_TYPE'].downcase.include?('s3') && ENV['S3_WRITEBACK_PATH'].present?
    end

    def local_path
      "#{ENV['PROJECT_DIR']}/writeback"
    end

    def filename(path)
      custom_file_names = {
        student_writebacks: ENV['CUSTOM_STUDENT_WRITEBACK_FILENAME'],
        message_writebacks: ENV['CUSTOM_MESSAGE_WRITEBACK_FILENAME'],
      }.with_indifferent_access

      name = custom_file_names[path]

      if name.blank?
        name = "fullmeasure_#{path}"
      end

      if ENV['ENABLE_WRITEBACK_FILENAME_TIMESTAMPS'] != '0'
        name = "#{name}_#{Time.now.to_i}"
      end
      "#{name}.csv"
    end

    def params
      @params ||= {}
    end
  end
end


