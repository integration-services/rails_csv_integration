class CreateCsvAttributes < ActiveRecord::Migration[5.2]
  def change
    create_table :csv_files do |t|
      t.string  :header_sha, index: true
      t.string  :file_sha, index: true
      t.string  :name, index: true
      t.string  :local_path
      t.string  :s3_path
      t.string  :fullmeasure_table_name, index: true
      t.string  :school_table_name, index: true
      t.string  :type, index: true
      t.timestamps
    end
  end
end
