class CreateAudiences < ActiveRecord::Migration[5.2]
  def change
    create_table :audiences do |t|
      t.string :status, index: true
      t.string :name, index: true
      t.integer :fullmeasure_id, index: true
      t.boolean :dirty, index: true
      t.datetime :last_attempt_to_push
      t.datetime :last_successful_push
      t.datetime :last_unsuccessful_push
      t.text :error_messages
      t.timestamps
    end
  end
end
