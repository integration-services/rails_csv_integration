class AddShaHistory < ActiveRecord::Migration[5.2]
  def change
    create_table(:audit_csv_files) do |t|
      t.integer :csv_file_id
      t.string :sha
      t.date :date
      t.timestamps
    end
  end
end
