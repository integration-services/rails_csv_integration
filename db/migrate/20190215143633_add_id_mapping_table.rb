class AddIdMappingTable < ActiveRecord::Migration[5.2]
  def change
    create_table :id_mappings do |t|
      t.string :contact_id_1, index: true
      t.string :contact_id_2, index: true
      t.string :source_1
      t.string :source_2
      t.timestamps
    end
  end
end
