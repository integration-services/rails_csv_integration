# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_15_143633) do

  create_table "audiences", force: :cascade do |t|
    t.string "status"
    t.string "name"
    t.integer "fullmeasure_id"
    t.boolean "dirty"
    t.datetime "last_attempt_to_push"
    t.datetime "last_successful_push"
    t.datetime "last_unsuccessful_push"
    t.text "error_messages"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dirty"], name: "index_audiences_on_dirty"
    t.index ["fullmeasure_id"], name: "index_audiences_on_fullmeasure_id"
    t.index ["name"], name: "index_audiences_on_name"
    t.index ["status"], name: "index_audiences_on_status"
  end

  create_table "audit_csv_files", force: :cascade do |t|
    t.integer "csv_file_id"
    t.string "sha"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "csv_files", force: :cascade do |t|
    t.string "header_sha"
    t.string "file_sha"
    t.string "name"
    t.string "local_path"
    t.string "s3_path"
    t.string "fullmeasure_table_name"
    t.string "school_table_name"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["file_sha"], name: "index_csv_files_on_file_sha"
    t.index ["fullmeasure_table_name"], name: "index_csv_files_on_fullmeasure_table_name"
    t.index ["header_sha"], name: "index_csv_files_on_header_sha"
    t.index ["name"], name: "index_csv_files_on_name"
    t.index ["school_table_name"], name: "index_csv_files_on_school_table_name"
    t.index ["type"], name: "index_csv_files_on_type"
  end

  create_table "id_mappings", force: :cascade do |t|
    t.string "contact_id_1"
    t.string "contact_id_2"
    t.string "source_1"
    t.string "source_2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id_1"], name: "index_id_mappings_on_contact_id_1"
    t.index ["contact_id_2"], name: "index_id_mappings_on_contact_id_2"
  end

end
