desc 'update the api from tables in sqlite database'
namespace :api do
  task upsert_audiences: [:environment] do
    Api::AudienceService.new.enqueue
  end

  task upsert_audience_contacts: [:environment] do
    Api::AudienceContactService.new.enqueue
  end

  task upsert_contacts: [:environment] do
    Api::ContactService.new.enqueue
  end
end
