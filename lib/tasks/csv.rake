desc 'upsert csvs into local sqlite database for easy extraction and comparison'
namespace :csv do
  task upsert_audiences: [:environment] do
    Csv::AudienceContactService.new.upsert
  end
  task upsert_contacts: [:environment] do
    Csv::ContactService.new.upsert
  end
end
