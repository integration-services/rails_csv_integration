desc 'update the sqlite database from s3'
namespace :db do
  task download: [:environment] do
    S3::DatabaseService.new.download
  end
  task upload: [:environment] do
    S3::DatabaseService.new.upload
  end
end
