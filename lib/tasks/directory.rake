desc 'create directory structure in build project directory'
namespace :directory do
  task create_structure: [:environment] do
    DirectoryService.new.create
  end
end
