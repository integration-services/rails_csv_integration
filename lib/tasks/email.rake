desc 'update the sqlite database from s3'
namespace :email do
  task errors: [:environment] do
    ErrorMailer.error(to: ENV['FTP_ADMIN_EMAIL'], gitlab_link: "https://gitlab.com/#{ENV['CI_PROJECT_PATH']}/-/jobs/#{ENV['CI_JOB_ID']}/artifacts/download").deliver_now!
    ErrorMailer.error(to: 'dev@fullmeasureed.com', gitlab_link: "https://gitlab.com/#{ENV['CI_PROJECT_PATH']}/-/jobs/#{ENV['CI_JOB_ID']}/artifacts/download").deliver_now!
  end
end
