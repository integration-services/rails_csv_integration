desc 'Create error artifacts'
namespace :encrypt do
  task files: [:environment] do
    EncryptService.new.encrypt
  end
end
