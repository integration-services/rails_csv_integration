desc 'Create error artifacts'
namespace :errors do
  task create_files: [:environment] do
    ErrorService.new.create
  end
end
