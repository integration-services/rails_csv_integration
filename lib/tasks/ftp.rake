desc 'ftp csvs from a remote source'
namespace :ftp do
  task download: [:environment] do
    Ftp::DownloadService.new.download
  end
end
