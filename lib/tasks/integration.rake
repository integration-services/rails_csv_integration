desc 'run all of the tasks'
namespace :integration do
  task run: [:environment] do
    IntegrationService.new.run
  end
end
