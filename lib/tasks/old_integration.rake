desc 'run all of the tasks'
namespace :old_integration do
  task run: [:environment] do

    puts 'creating directory structure'
    Rake::Task['directory:create_structure'].execute

    puts 'downloading database and migrating'
    Rake::Task['db:download'].execute
    Rake::Task['db:create'].execute
    Rake::Task['db:migrate'].execute

    puts 'downloading csvs from remote source'
    Rake::Task['ftp:download'].execute if ENV['FTP_TYPE'].downcase == 'ftp'
    Rake::Task['s3:download'].execute if ENV['FTP_TYPE'].downcase == 's3'

    puts 'upserting csvs into database'
    Rake::Task['csv:upsert_contacts'].execute
    Rake::Task['csv:upsert_audiences'].execute

    puts 'upserting data into ice via web services apis'
    Rake::Task['api:upsert_contacts'].execute
    Rake::Task['api:upsert_audiences'].execute

    puts 'upload database to s3'
    Rake::Task['db:upload'].execute

    puts 'archiving data errors'
    Rake::Task['errors:create_files'].execute
    Rake::Task['encrypt:files'].execute

    puts 'emailing data errors'
    Rake::Task['email:errors'].execute
  end
end
