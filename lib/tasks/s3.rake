desc 'download csvs from a remote s3 bucket'
namespace :s3 do
  task download: [:environment] do
    S3::DownloadService.new.download
  end
end
