desc 'Place writebacks in remote source'
namespace :writeback do
  task upload: [:environment] do
    S3::WritebackService.new.download_writebacks
    Ftp::WritebackService.new.upload if ENV['FTP_TYPE'].downcase == 'ftp'
    S3::WritebackService.new.upload if ENV['FTP_TYPE'].downcase == 's3'
  end
end
